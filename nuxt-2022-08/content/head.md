<div class="mx-4 my-5 my-5 text-center mt-0">
  
  <h1 class="display-5 fw-bold">Nezávislí pro Plasko</h1>
  <h5>Plasy • Babina • H. Hradiště • Žebnice • Nebřeziny • Lomnička</h5>
    <img class="d-block mx-auto mb-4 mt-2" src="/logo_48x42.png" alt="" width="48" height="42"></img>

  <h1>Jdeme do toho znovu</h1>
  <div class="col-lg-6 mx-auto">
    <p class="lead mb-4">Musíme řešit problémy, které současné vedení radnice (ČSSD spolu s “MY” a ODS) neustále odsouvá.</p>
    <div class="d-grid gap-2 d-sm-flex justify-content-sm-center">
      <!-- <button type="button" class="btn btn-primary btn-lg px-4 gap-3">Primary button</button>
      <button type="button" class="btn btn-outline-secondary btn-lg px-4">Secondary</button> -->
    </div>
  </div>
</div>