  <div class="px-4 pt-5 text-center border-bottom">
    <h1 class="fw-bold"><small>Zdražující se poplatky za odpady</small></h1>
    <div class="col-lg-6 mx-auto">
      <p class="lead mb-4">umožníme výběr frekvence vývozu popelnic: 1x týdně, 1x 14 dní, atd.</p>
      <div class="d-grid gap-2 d-sm-flex justify-content-sm-center mb-5">
        <!-- <button type="button" class="btn btn-primary btn-lg px-4 me-sm-3">Primary button</button>
        <button type="button" class="btn btn-outline-secondary btn-lg px-4">Secondary</button> -->
      </div>
    </div>
    <div class="overflow-hidden" style="max-height: 40vh;">
      <div class="container pmx-5">
        <img src="/popelnice.jpg" class="img-fluid border rounded-3 shadow-lg mb-4" alt="Example image" width="200" height="100" loading="lazy">
      </div>
    </div>
  </div>