<div class="container col-xxl-8 mx-4">
  <div class="row flex-lg-row-reverse align-items-center g-5 py-5">
    <div class="col-10 col-sm-8 col-lg-6">
      <img src="/school.webp" class="d-block mx-lg-auto img-fluid" alt="Bootstrap Themes" width="200" height="200" loading="lazy">
    </div>
    <div class="col-lg-6">
      <h1 class="fw-bold lh-1 mb-3"><small>Nový pavilon základní školy</small></h1>
      <p class="lead">ZŠ praská ve švech, sice je to prý “priorita” vedení radnice, ale to už jsme slyšeli před 4 lety a nic nestojí.</p>
      <div class="d-grid gap-2 d-md-flex justify-content-md-start">
        <!-- <button type="button" class="btn btn-primary btn-lg px-4 me-md-2">Primary</button>
        <button type="button" class="btn btn-outline-secondary btn-lg px-4">Default</button> -->
      </div>
    </div>
  </div>
</div>