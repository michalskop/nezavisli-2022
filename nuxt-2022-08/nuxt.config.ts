import { defineNuxtConfig } from 'nuxt'

// https://v3.nuxtjs.org/api/configuration/nuxt.config
export default defineNuxtConfig({

  modules: [
    // https://content.nuxtjs.org/get-started
    '@nuxt/content'
  ],
  content: {
    // https://content.nuxtjs.org/api/configuration  
  }

})
